import React from "react";
import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware } from "redux";
import listdataReducer from "./store/reducers/listdata";

import ReduxThunk from "redux-thunk";

import AppNavigator from "./navigation/AppNavigator";

const rootReducer = combineReducers({
  listdata: listdataReducer,
});
const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

export default function App() {
  return (
    <Provider store={store}>
      <AppNavigator />
    </Provider>
  );
}
