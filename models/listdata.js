class ListData {
  constructor(
    _id,
    name,
    imageUrl,
    url,
    films,
    shortFilms,
    tvShows,
    videoGames,
    parkAttractions,
    allies,
    enemies,
    date
  ) {
    this.id = _id;
    this.name = name;
    this.imageUrl = imageUrl;
    this.url = url;
    this.films = films;
    this.shortFilms = shortFilms;
    this.tvShows = tvShows;
    this.videoGames = videoGames;
    this.parkAttractions = parkAttractions;
    this.allies = allies;
    this.enemies = enemies;
    this.date = date;
  }
}
export default ListData;
