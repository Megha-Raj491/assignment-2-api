import React from "react";
import { View, StyleSheet, TouchableNativeFeedback } from "react-native";

const ListGridItem = (props) => {
  return (
    <TouchableNativeFeedback onPress={props.onSelect}>
      <View style={{ ...styles.card, ...props.style }}>{props.children}</View>
    </TouchableNativeFeedback>
  );
};
const styles = StyleSheet.create({
  card: {
    elevation: 10,
    backgroundColor: "#ff7f50",
    padding: 20,
    borderRadius: 20,
  },
});
export default ListGridItem;
