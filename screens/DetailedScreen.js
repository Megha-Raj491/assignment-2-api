import React, { useEffect } from "react";
import { StyleSheet, View, Text, Image, ScrollView } from "react-native";
import { useSelector } from "react-redux";
import ListGridItem from "../components/ListGridItem";
import AsyncStorage from "@react-native-async-storage/async-storage";

const DetailedScreen = (props) => {
  const characterId = props.navigation.getParam("CatId");
  const characterName = props.navigation.getParam("CatName");
  const characterImage = props.navigation.getParam("CatImage");
  const characterFilms = props.navigation.getParam("CatFilms");
  const characterShort = props.navigation.getParam("CatShort");
  const characterVideo = props.navigation.getParam("CatVideoGame");
  const characterTv = props.navigation.getParam("CatTv");

  const detailsForStore = useSelector((state) =>
    state.listdata.listItem.find((char) => char.id === characterId)
  );

  const setHistData = async () => {
    detailsForStore.date = new Date().toLocaleString();
    let storedData = [];

    const details = await AsyncStorage.getItem("characterDetails");
    const fetchDetails = JSON.parse(details);

    if (fetchDetails != null) {
      storedData = [...fetchDetails, detailsForStore];
      await AsyncStorage.setItem(
        "characterDetails",
        JSON.stringify(storedData)
      );
    } else {
      storedData.push(detailsForStore);

      await AsyncStorage.setItem(
        "characterDetails",
        JSON.stringify(storedData)
      );
    }
  };
  useEffect(() => {
    setHistData();
  }, []);

  return (
    <ScrollView>
      <View>
        <ListGridItem style={styles.detailedGrid}>
          <View>
            <View style={styles.imagePosition}>
              <Image
                source={{ uri: characterImage }}
                style={styles.imageStyle}
              />
              <View>
                <Text style={styles.titleSytle}>
                  {characterName.toUpperCase()}
                </Text>
              </View>
            </View>
          </View>
        </ListGridItem>

        {characterFilms.length != 0 ? (
          <View>
            <View style={styles.description}>
              <Text style={styles.descriptionHead}>Films</Text>
            </View>
            {characterFilms.map((list) => (
              <Text key={list} style={styles.listStyle}>
                {list}
              </Text>
            ))}
          </View>
        ) : (
          <View>
            <Text></Text>
          </View>
        )}

        {characterShort.length != 0 ? (
          <View>
            <View style={styles.description}>
              <Text style={styles.descriptionHead}>Short Films</Text>
            </View>
            {characterShort.map((list) => (
              <Text key={list} style={styles.listStyle}>
                {list}
              </Text>
            ))}
          </View>
        ) : (
          <View>
            <Text></Text>
          </View>
        )}

        {characterTv.length != 0 ? (
          <View>
            <View style={styles.description}>
              <Text style={styles.descriptionHead}>TV Shows</Text>
            </View>
            {characterTv.map((list) => (
              <Text key={list} style={styles.listStyle}>
                {list}
              </Text>
            ))}
          </View>
        ) : (
          <View>
            <Text></Text>
          </View>
        )}

        {characterVideo.length != 0 ? (
          <View>
            <View style={styles.description}>
              <Text style={styles.descriptionHead}>videoGames</Text>
            </View>
            {characterVideo.map((list) => (
              <Text key={list} style={styles.listStyle}>
                {list}
              </Text>
            ))}
          </View>
        ) : (
          <View>
            <Text></Text>
          </View>
        )}

        {characterFilms.length === 0 &&
        characterTv.length === 0 &&
        characterShort.length === 0 &&
        characterVideo.length === 0 ? (
          <View>
            <View style={styles.description}>
              <Text style={styles.listEmptyDescription}>
                Here have no datas !
              </Text>
            </View>
          </View>
        ) : (
          <View>
            <Text></Text>
          </View>
        )}
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  imageStyle: {
    width: "100%",
    height: "80%",
    marginTop: 10,
    resizeMode: "contain",
  },
  imagePosition: {
    width: "100%",
    height: "100%",
  },
  detailedGrid: {
    borderRadius: 1,
    height: 370,
    backgroundColor: "#ffdab9",
  },
  titleSytle: {
    fontSize: 24,
    textAlign: "center",
    color: "purple",
    marginTop: 10,
    fontWeight: "bold",
    letterSpacing: 5,
  },
  descriptionHead: {
    fontSize: 22,
    textAlign: "justify",
    marginVertical: 10,
  },
  description: {
    justifyContent: "center",
    alignItems: "center",
  },
  listStyle: {
    fontSize: 12,
    marginLeft: 20,
    marginTop: 10,
  },
  listEmptyDescription: {
    fontSize: 14,
    letterSpacing: 10,
    marginTop: 20,
  },
});

export default DetailedScreen;
