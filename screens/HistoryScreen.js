import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text, FlatList } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import ListGridItem from "../components/ListGridItem";

import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";

const HistoryScreen = (props) => {
  const [viewedData, setViewedData] = useState();

  const getDataFromStorage = async () => {
    const getData = await AsyncStorage.getItem("characterDetails");

    const newGetData = JSON.parse(getData);

    setViewedData(newGetData);
  };

  useEffect(() => {
    getDataFromStorage();
  }, [viewedData]);

  const HistListDataHandler = (itemData) => {
    return (
      <View style={styles.topView}>
        <View>
          <ListGridItem style={styles.gridViewStyle}>
            <Text style={styles.dateStyle}>{itemData.item.date}</Text>
            <Text style={styles.nameStyle}>{itemData.item.name}</Text>
          </ListGridItem>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.histListScreen}>
      <FlatList
        inverted
        data={viewedData}
        keyExtractor={(item) => item.date}
        renderItem={HistListDataHandler}
      />
    </View>
  );
};
HistoryScreen.navigationOptions = (navData) => {
  return {
    headerTitle: "History",
    headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu-Id"
          iconName={Platform.OS === "android" ? "md-menu" : "ios-menu"}
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
  };
};
const styles = StyleSheet.create({
  histListScreen: {
    flex: 1,
    backgroundColor: "black",
  },
  histListstyle: {
    marginVertical: 15,
  },
  gridViewStyle: {
    width: "80%",
    height: 100,
    marginVertical: 10,
    marginLeft: 30,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#deb887",
  },
  topView: {
    marginTop: 20,
  },
  dateStyle: {
    fontSize: 20,
    color: "red",
  },
  nameStyle: {
    fontSize: 16,
    color: "#ff8c00",
    fontWeight: "bold",
  },
});

export default HistoryScreen;
