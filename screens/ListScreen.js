import React, { useEffect } from "react";
import { StyleSheet, View, Text, FlatList } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import ListGridItem from "../components/ListGridItem";
import * as listdataActions from "../store/actions/listdata";

import { HeaderButtons, Item } from "react-navigation-header-buttons";

import HeaderButton from "../components/HeaderButton";

const ListScreen = (props) => {
  const listDataItems = useSelector((state) => state.listdata.listItem);

  const dispatch = useDispatch();

  const listDataHandler = (itemData) => {
    return (
      <View>
        <ListGridItem
          style={styles.gridViewStyle}
          onSelect={() => {
            props.navigation.navigate({
              routeName: "Details",
              params: {
                CatId: itemData.item.id,
                CatName: itemData.item.name,
                CatImage: itemData.item.imageUrl,
                CatFilms: itemData.item.films,
                CatShort: itemData.item.shortFilms,
                CatVideoGame: itemData.item.videoGames,
                CatTv: itemData.item.tvShows,
              },
            });
          }}
        >
          <Text>{itemData.item.name}</Text>
        </ListGridItem>
      </View>
    );
  };

  useEffect(async () => {
    await dispatch(listdataActions.fetch_data());
  }, []);
  return (
    <View>
      <FlatList
        numColumns={2}
        data={listDataItems}
        keyExtractor={(item) => item.id}
        renderItem={listDataHandler}
      />
    </View>
  );
};
ListScreen.navigationOptions = (navData) => {
  return {
    headerTitle: "Characters",
    headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName={Platform.OS === "android" ? "md-menu" : "ios-menu"}
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
  };
};
const styles = StyleSheet.create({
  listpage: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ffe4b5",
  },
  gridViewStyle: {
    width: 170,
    height: 160,
    marginVertical: 10,
    marginLeft: 10,
    marginRight: 10,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ff6347",
  },
});

export default ListScreen;
