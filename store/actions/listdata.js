import ListData from "../../models/listdata";
export const SET_DATA = "SET_DATA";

export const fetch_data = () => {
  return async (dispatch) => {
    const response = await fetch("https://api.disneyapi.dev/characters");

    const resData = await response.json();

    const loadedData = [];
    for (const key in resData.data) {
      loadedData.push(
        new ListData(
          resData.data[key]._id,
          resData.data[key].name,
          resData.data[key].imageUrl,
          resData.data[key].url,
          resData.data[key].films,
          resData.data[key].shortFilms,
          resData.data[key].tvShows,
          resData.data[key].videoGames,
          resData.data[key].parkAttractions,
          resData.data[key].allies,
          resData.data[key].enemies
        )
      );
    }

    dispatch({ type: SET_DATA, listdata: loadedData });
  };
};
