import { SET_DATA } from "../actions/listdata";

const initialState = {
  listItem: [],
};

const listdataReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATA:
      return { ...state, listItem: action.listdata };

    default:
      return state;
  }
};

export default listdataReducer;
