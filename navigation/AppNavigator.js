import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
import DetailedScreen from "../screens/DetailedScreen";
import ListScreen from "../screens/ListScreen";
import HistoryScreen from "../screens/HistoryScreen";

const AppNavigation = createStackNavigator({
  List: {
    screen: ListScreen,
    navigationOptions: {
      headerStyle: {
        backgroundColor: "orange",
      },
    },
  },
  Details: {
    screen: DetailedScreen,
    navigationOptions: {
      headerStyle: {
        backgroundColor: "orange",
      },
    },
  },
});
const HistoryNavigation = createStackNavigator({
  History: {
    screen: HistoryScreen,
    navigationOptions: {
      headerStyle: {
        backgroundColor: "orange",
      },
    },
  },
});
const MenuNavigation = createDrawerNavigator(
  {
    Characters: AppNavigation,
    History: HistoryNavigation,
  },
  {
    contentOptions: {
      labelStyle: {
        marginTop: 30,
      },
    },
  }
);
export default createAppContainer(MenuNavigation);
